﻿<?php
/***********************************************/
/*                                             */
/*  SKRIPT ZUM EXPORT DER VERSANDDATEN         */
/*                                             */
/*  INPUT  : Daten aus MSSQL-Datenbank         */
/*  OUTPUT : XML zum Import bei ITC            */
/*                                             */
/*  Datum  : 25.11.2019                        */
/*  Version: 4.8                               */
/*                                             */
/***********************************************/

//Fehlerausgabe aktivieren
error_reporting(E_ALL);
//Richtige Zeitzone setzen
date_default_timezone_set('Europe/Berlin');
echo 'START'.PHP_EOL;

include("config.php");

//Prüfe gültige Verzeichnisse
if (!is_dir($xml_file_path)) die("Ungültiges XML-Verzeichnis (".$xml_file_path."). Bitte config.php überprüfen!"); 
if (!is_dir($ls_file_path)) die("Ungültiges Lieferschein-Verzeichnis (".$ls_file_path."). Bitte config.php überprüfen!"); 
if (!is_dir($xml_archive)) die("Ungültiges XML-Archiv (".$xml_archive."). Bitte config.php überprüfen!"); 
if (!is_dir($ls_archive)) die("Ungültiges Lieferschein-Archiv (".$ls_archive."). Bitte config.php überprüfen!"); 

//Initialisierung
define("version", "4.8");
define("xml_file_path", $xml_file_path);
define("ls_file_path", $ls_file_path);
define("xml_archive", $xml_archive);
define("ls_archive", $ls_archive);

$output=array();
$data=array();
$temp_partlist=array();
$data_i=0;

//Verbindung zur MSSQL-Datenbank
try 
{
    //Linux
    //$db = new PDO("dblib:host=".$mssql['server'].":".$mssql['port'].";dbname=".$mssql['database'],$mssql['user'],$mssql['password']);
    //Windows
    $db = new PDO("sqlsrv:Server=".$mssql['server'].(ctype_digit($mssql['port']) ? ','.$mssql['port'] : '').";Database=".$mssql['database'],$mssql['user'],$mssql['password']);
} 
catch (PDOException $e) 
{
    echo "Failed to get DB handle: " . $e->getMessage() . "\n";
    exit;
}

if ($handle = opendir(ls_file_path)) {
  	while (false !== ($entry = readdir($handle))) {
		echo substr($entry,0,10).PHP_EOL;
        if ($entry != "." && $entry != ".." && substr($entry,0,10) == 'Packzettel') {
            $entry_org=$entry;
            $entry=trim(str_replace('Packzettel_','',$entry));
			echo "$entry\n";
			$ls=pathinfo(ls_file_path.DIRECTORY_SEPARATOR.$entry);
			if (export($ls['filename'],$db) == 'ok') {
				//XML generiert, verschiebe PDF ins Archiv
				if (!rename(ls_file_path.DIRECTORY_SEPARATOR.$entry_org, ls_archive.DIRECTORY_SEPARATOR.$entry_org)) {
					echo "Archivierung von ".ls_file_path.DIRECTORY_SEPARATOR.$entry_org." fehlgeschlagen".PHP_EOL;
				}
			}
			else {
				if (substr($entry_org,0,10) == 'Packzettel') echo 'XML fuer das PDF "'.$entry_org.'" konnte nicht generiert werden!'.PHP_EOL;
			}
        }
    }
    closedir($handle);
}
echo 'ENDE'.PHP_EOL;
exit;
	
function export($auftrag, $db) {
	$error='';
    //Hole Auftragsdaten
    $sql=<<<SQL
    SELECT
        GOODDOCUMENT.DOCUMENTNUMBER AS INTERNERLIEFERSCHEIN,
		KUNDE.INTERNALNUMBER AS AUFTRAGSREFERENZ,
		DOSSIERSET.DOSSIERCHARACTERIZATION AS INFO,
		DOCUMENTHEAD.NAME12 AS NAME1,
	    CASE WHEN DOCUMENTHEAD.NAME22 = '' THEN DOCUMENTHEAD.FIELD02 ELSE DOCUMENTHEAD.NAME22 END AS NAME2,
	    CASE WHEN DOCUMENTHEAD.NAME22 = '' THEN '' ELSE DOCUMENTHEAD.FIELD02 END AS NAME3,
		DOCUMENTHEAD.STREETPOSTOFFICE2 AS STRASSE,
		DOCUMENTHEAD.CITY2 AS ORT,
		DOCUMENTHEAD.LANDBEZ2 AS LAND,
		DOCUMENTHEAD.FIELD03 AS ANSPRECHPARTNER,
		DOCUMENTHEAD.FIELD12 AS ANSPRECHPARTNER_EMAIL,
		'' AS ANSPRECHPARTNER_TELEFON,
	    DOCUMENTHEAD.FIELD29 AS ANSPRECHPARTNER_FAX,
		GOODDOCUMENTITEM.ARTIKELNR AS STUECKLISTE,
        GOODDOCUMENTITEM.ITEMIDENTNR AS PZ_POS,
        GOODDOCUMENTITEM.ITEMTEXT AS PZ_TEXT,
        GOODDOCUMENTITEM.QUANTITY AS STUECKLISTE_MENGE,
        GOODDOCUMENTITEM.MENGENEINHEIT AS STUECKLISTE_MENGENEINHEIT,
		ARTIKELVERPACKUNGSZUORD.BREITE,
	    ARTIKELVERPACKUNGSZUORD.HOEHE,
	    ARTIKELVERPACKUNGSZUORD.GEWICHT,
		ARTIKEL.FIELD11 AS VERPACKUNGSGEWICHT,
	    ARTIKEL.FIELD12 AS VERSANDGEWICHT,
	    ARTIKEL.FIELD10 AS ARTIKEL_TEXT,
	    ARTIKEL.FIELD13 AS ARTIKEL_ABMESSUNG,
	    ARTIKEL.KZLAGERGEFUEHRT AS LAGERGEFUEHRT
     FROM GOODDOCUMENT
	     ,DOSSIERSET
         ,GOODDOCUMENTITEM
		 ,ARTIKELVERPACKUNGSZUORD 
		 ,DOCUMENTHEAD	
         ,KUNDE		 
         ,ARTIKEL
     WHERE GOODDOCUMENT.DOCUMENTNUMBER = ?
       AND GOODDOCUMENT.DOCUMENTNUMBER = GOODDOCUMENTITEM.DOCUMENTNUMBER
       AND GOODDOCUMENT.DOCUMENTNUMBER = DOCUMENTHEAD.DOCUMENTNUMBER
	   AND GOODDOCUMENT.DOSSIERNUMBER=DOSSIERSET.DOSSIERNUMBER 
	   AND GOODDOCUMENT.PERSONENNR = KUNDE.PERSONENNR
	   AND GOODDOCUMENTITEM.ARTIKELNR=ARTIKELVERPACKUNGSZUORD.ARTIKELNR
	   AND GOODDOCUMENTITEM.ARTIKELNR=ARTIKEL.ARTIKELNR	
	   AND GOODDOCUMENTITEM.QUANTITY > 0
     ORDER BY GOODDOCUMENTITEM.ITEMIDENTNR	   
SQL;
    
    $sth=$db->prepare($sql);
    if (!$sth->execute(array($auftrag))) {
      echo "ERROR".PHP_EOL;
      print_r($sth->errorInfo());
	  return 'nok';
    }
    
    $data=$sth->fetchAll();
    $counter=1;
    foreach ($data as $row) {
		//Ort, PLZ und Land voneinander trennen
		$value=trim($row['ORT']);
		$parts=explode(" ",$value);
		if (count($parts) < 2) die ("Ort,PLZ und Land konnten nicht ermittelt werden #1");
		if (count($parts) == 2) {
			$ort=$parts[1];
		}
		//Blanks in Ortsname
		if (count($parts) > 2) {
            $ort=$parts[1];
        	for ($t=2; $t<count($parts); $t++) {
        		$ort.=' '.$parts[$t];
        	}
        }
		$part=explode("-",$parts[0]);
//		if (count($part) != 2) die ("PLZ und Land konnten nicht ermittelt werden #2");
//		$land=$part[0];
//		$plz=$part[1];

		$land=$part[0];
		if(strtolower($part[0]) == 'pl') {
			$plz = $part[1] . '-' . $part[2];
		} else {
			$plz = $part[1];
		}

    	$row['INTERNERLIEFERSCHEIN']=trim($row['INTERNERLIEFERSCHEIN']);
    	$output['internerlieferschein']=$row['INTERNERLIEFERSCHEIN'];
    	$output['auftragsreferenz']=$row['AUFTRAGSREFERENZ'];
    	$output['info']=$row['INFO'];
    	$output['name1']=trim($row['NAME1']);
		$output['name2']=trim($row['NAME2']);
    	$output['name3']=trim($row['NAME3']);
		//Check address data START 20191125
		if (strlen($output['name1']) == 0) {
			if (strlen($output['name2']) == 0) {
				if (strlen($output['name3']) == 0) {
					echo 'Address data incomplete for order:'.$output['internerlieferschein'].' / '.$output['auftragsreferenz'];
					return 'nok';
				}
				else {
					$output['name1']=$output['name3'];
					$output['name3']='';
				}
			}
			else {
				$output['name1']=$output['name2'];
				$output['name2']='';
			}
		}
		//Check address data END 20191125
		$output['strasse']=trim($row['STRASSE']);
		$output['ort']=$ort;
    	$output['plz']=$plz;
    	$output['land']=$land;
    	$output['ansprechpartner']=trim($row['ANSPRECHPARTNER']);
    	$output['ansprechpartner_email']=trim($row['ANSPRECHPARTNER_EMAIL']);
    	$output['ansprechpartner_telefon']=trim($row['ANSPRECHPARTNER_TELEFON']);
    	$output['ansprechpartner_fax']=trim($row['ANSPRECHPARTNER_FAX']);
    	//if (isset($output[$row['INTERNERLIEFERSCHEIN']]['items'])) $art_count=count($output[$row['INTERNERLIEFERSCHEIN']]['items']) + 1;
    	//else $art_count=0;
    	//$output[$row['INTERNERLIEFERSCHEIN']]['items'][$art_count]['artikelnr']=trim($row['PZ_ARTIKEL']);
    	//$output[$row['INTERNERLIEFERSCHEIN']]['items'][$art_count]['position']=$row['PZ_POS'];
    	//$output[$row['INTERNERLIEFERSCHEIN']]['items'][$art_count]['menge']=round($row['PZ_MENGE']);
		$output['items'][$row['PZ_POS']]['pz_artikelnr']=$row['STUECKLISTE'];
		$output['items'][$row['PZ_POS']]['pz_pos']=$row['PZ_POS'];
		$output['items'][$row['PZ_POS']]['pz_text']=rtf2txt($row['PZ_TEXT']);
		$output['items'][$row['PZ_POS']]['stueckliste_menge']=$row['STUECKLISTE_MENGE'];
		$output['items'][$row['PZ_POS']]['stueckliste_mengeneinheit']=$row['STUECKLISTE_MENGENEINHEIT'];
		$output['items'][$row['PZ_POS']]['breite']=$row['BREITE'];
		$output['items'][$row['PZ_POS']]['hoehe']=$row['HOEHE'];
		$output['items'][$row['PZ_POS']]['gewicht']=$row['GEWICHT'];
		$output['items'][$row['PZ_POS']]['verpackungsgewicht']=$row['VERPACKUNGSGEWICHT'];
		$output['items'][$row['PZ_POS']]['versandgewicht']=$row['VERSANDGEWICHT'];
		$output['items'][$row['PZ_POS']]['artikel_text']=$row['ARTIKEL_TEXT'];
		$output['items'][$row['PZ_POS']]['artikel_abmessung']=$row['ARTIKEL_ABMESSUNG'];
    	$temp_partlist=get_partlist(trim($row['STUECKLISTE']),$counter,$db);
		//$counter=count($output[$row['INTERNERLIEFERSCHEIN']]['items']);
		//Stücklistenartikel ist der Artikel; keine "Unterartikel"
		if ((count($temp_partlist) == 0) and ($row['LAGERGEFUEHRT'] == 1)) {
			$temp_partlist[0]['pl_pos']=$counter;
			$temp_partlist[0]['pl_child']=$row['STUECKLISTE'];
		    $temp_partlist[0]['pl_menge']=1;
		}
		for ($i=0; $i<count($temp_partlist); $i++) {
		   	$output['items'][$row['PZ_POS']]['artikel'][$counter]=$temp_partlist[$i];
		   	$counter++;
		}
    }
	
	//print_r($output);
    
    //Generiere XML-File
    
    $order = new SimpleXMLElement("<?xml version=\"1.0\"?><export></export>");
    $order->addAttribute("v",version);
    array_to_xml($output,$order);
	$file_name='EXPORT_'.$auftrag.'_'.date("Ymd_His").'.xml';
    $file=xml_file_path.DIRECTORY_SEPARATOR.$file_name;
    echo 'Writing XML to: "'.$file.'"'.PHP_EOL;
    $xml_file = $order->asXML($file);
    if($xml_file){
        echo 'XML file for '.$auftrag.' has been generated successfully.';
    	return 'ok';
    }
    else{
        echo 'XML file for '.$auftrag.' with generation error.';
    	return 'nok';
    }
}

function get_partlist ($artikel,$counter,$db) {
	
	$sql=<<<SQL
		WITH items (PL_ORDER,PL_PARTLIST,PL_ARTIKEL,CHECK_ART,PL_MENGE,PL_EINHEIT,PL_LAGERGEFUEHRT,PL_BEZEICHNUNG,PL_ZUORDNUNG,PL_LEVEL) AS
          (
          SELECT           
               PL1.NUMERICALORDER AS PL_ORDER,          
               PL1.PARTSLISTNR AS PL_PARTLIST,          
               PL1.ARTIKELNR AS PL_ARTIKEL,          
               ART1.ARTIKELNR AS CHECK_ART,          
               CAST(PL1.QUANTITY AS INT) AS PL_MENGE ,          
               PL1.MENGENEINHEIT AS PL_EINHEIT,          
               ART1.KZLAGERGEFUEHRT AS PL_LAGERGEFUEHRT,           
               G1.BEZEICHNUNG AS PL_BEZEICHNUNG,          
               Z1.ZUORDNUNGSNR AS PL_ZUORDNUNG,
               CAST(0 AS int) AS PL_LEVEL          
           FROM PARTSLIST PL1          
               ,ARTIKEL ART1          
               ,ARTIKELGRUPPENZUORDNUNG Z1          
               ,GRUPPEN G1          
          WHERE PL1.PARTSLISTNR = ?          
            AND PL1.ARTIKELNR = ART1.ARTIKELNR          
            AND ART1.ARTIKELNR = Z1.ARTIKELNR         
            AND Z1.ZUORDNUNGSNR = G1.ZUORDNUNGSNR         
            AND G1.BEZEICHNUNG = 'Produktk' AND NOT Z1.ZUORDNUNGSNR IN ('00000251','00000963','00000649','00000964','00000244','00000253')          
          UNION ALL          
          SELECT           
               PL2.NUMERICALORDER AS PL_ORDER,          
               PL2.PARTSLISTNR AS PL_PARTLIST,          
               PL2.ARTIKELNR AS PL_ARTIKEL,          
               ART2.ARTIKELNR AS CHECK_ART,          
               CAST(PL2.QUANTITY AS INT) * CAST(A.PL_MENGE AS INT) AS PL_MENGE ,          
               PL2.MENGENEINHEIT AS PL_EINHEIT,          
               ART2.KZLAGERGEFUEHRT AS PL_LAGERGEFUEHRT,           
               G2.BEZEICHNUNG AS PL_BEZEICHNUNG,          
               Z2.ZUORDNUNGSNR AS PL_ZUORDNUNG,         
               PL_LEVEL + 1          
           FROM PARTSLIST PL2          
               ,ARTIKEL ART2          
               ,ARTIKELGRUPPENZUORDNUNG Z2          
               ,GRUPPEN G2          
               ,items A          
          WHERE A.PL_ARTIKEL= PL2.PARTSLISTNR          
            AND PL2.ARTIKELNR = ART2.ARTIKELNR          
            AND ART2.ARTIKELNR = Z2.ARTIKELNR          
            AND Z2.ZUORDNUNGSNR = G2.ZUORDNUNGSNR          
            AND G2.BEZEICHNUNG = 'Produktk' AND NOT Z2.ZUORDNUNGSNR IN ('00000251','00000963','00000649','00000964','00000244','00000253')            
          )          
          SELECT PL_ORDER,PL_PARTLIST,PL_ARTIKEL,CHECK_ART,PL_MENGE,PL_EINHEIT,PL_LAGERGEFUEHRT,PL_BEZEICHNUNG,PL_ZUORDNUNG,PL_LEVEL          
            FROM items
           WHERE PL_LAGERGEFUEHRT = 1          
           ORDER BY PL_LEVEL, PL_ORDER          
          OPTION (MAXRECURSION 5)
SQL;
	$sth=$db->prepare($sql);
	if (!$sth->execute(array($artikel))) {
		echo "ERROR".PHP_EOL;
		print_r($sth->errorInfo());
	}
	$art=$sth->fetchAll();
	$error=$sth->errorInfo();
	if ($error[1] <> 0) print_r($sth->errorInfo());
	$partlist=array();
	$i=0;
	foreach ($art as $row) {
		//$partlist[$i]['level']=$row['PL_LEVEL'];
		//$partlist[$i]['pl_order']=$row['PL_ORDER'];
		//$partlist[$i]['pl_artikel']=$row['PL_PARTLIST'];
		$partlist[$i]['pl_pos']=$counter;
		$partlist[$i]['pl_child']=$row['PL_ARTIKEL'];
		//$partlist[$i]['pl_check']=$row['CHECK_ART'];
		$partlist[$i]['pl_menge']=round($row['PL_MENGE']);
		//$partlist[$i]['pl_einheit']=$row['PL_EINHEIT'];
		//$partlist[$i]['pl_lagergefuehrt']=$row['PL_LAGERGEFUEHRT'];
		//$partlist[$i]['pl_bezeichnung']=$row['PL_BEZEICHNUNG'];
		//$partlist[$i]['pl_zuordnung']=$row['PL_ZUORDNUNG'];	
		$i++;
		$counter++;
	}	
	return $partlist;
}

function rtf2txt($str) {
	$string = preg_replace("/(\{.*\})|}|(\\\\\S+ ?)/", "", $str);
	return $string;
}

function array_to_xml($array, &$order) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $order->addChild("$key");
                array_to_xml($value, $subnode);
            }else{
				$n=$order->getName();
                if ($n == 'items') $subnode = $order->addChild("pz_artikel");
                elseif ($n == 'artikel') $subnode = $order->addChild("child_artikel");
				else $subnode = $order->addChild($n);
                array_to_xml($value, $subnode);
            }
        }else {
            $order->addChild("$key",htmlspecialchars("$value"));
        }
    }
}


?>
